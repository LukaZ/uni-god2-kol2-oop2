#include <iostream>
#include <fstream> /** Zaglavlje biblioteke za rad sa file stream-ovima. */

#include "engine.h"
#include "level.h"
#include "sprite.h"

using namespace std;

/** \brief Funkcija od koje otpočinje izvršavanje programa.
 *
 * \param argc Broj ulaznih argumenata.
 * \param argv Niz stringova koji predstavljaju ulazne argumente.
 * \return Ceo broj koji predstavlja kod greške. Ukoliko je vraćena vrednost 0 znači da nije došlo do greške.
 *
 */
int main(int argc, char** argv)
{
    Engine *eng = new Engine("RPG Game");
    eng->addTileset("resources/tilesets/grass_tileset.txt", "default"); /** Dodavanje novog tileset-a. */

    ifstream levelStream("resources/levels/level1.txt");
    eng->addDrawable(new Level(levelStream, eng->getTileset("default"))); /** Dodavanje novog nivoa. */

    eng->run(); /** Pokretanje glavne petlje igre. */
    delete eng; /** Oslobadjanje memorije koju je zauzela instanca Engine klase. */
    return 0;
}
