#ifndef REVERSEDSPRITE_H_INCLUDED
#define REVERSEDSPRITE_H_INCLUDED

#include "sprite.h"
#include "fstream"
#include <sstream>

class ReversedSprite: public Sprite
{
    public:
        float zamor = 0;
        bool isCoolingDown = false;
        ReversedSprite(SpriteSheet *sheet, int width, int height):Sprite(sheet, width, height)
        {
            this->modifyPosition();
        };

        void move(int dx, int dy) {

            spriteRect->x += dx;
            spriteRect->y += dy;


            if( !isCoolingDown )
            {
                zamor += 0.5;
            }


            if (zamor >= 100 )
            {
                isCoolingDown = true;
            }
            else if ( zamor <= 0 )
            {
                isCoolingDown = false;
            }
            //cout << zamor << endl;
        };


        void move() {

            if(state != 0 && !isCoolingDown) {
                if(state & 2) {
                    move(-1, 0);
                }
                if(state & 1) {
                    move(1, 0);
                }
                if(state & 8) {
                    move(0, -1);
                }
                if(state & 4) {
                    move(0, 1);
                }
            }
            else {
                if(state & 1) {
                    move(-1, 0);
                }
                if(state & 2) {
                    move(1, 0);
                }
                if(state & 4) {
                    move(0, -1);
                }
                if(state & 8) {
                    move(0, 1);
                }
            }
        };

        void recordPosition()
        {
            ofstream myfile ("resources/pozicija.txt");
            if (myfile.is_open())
            {
                myfile << "\n";
                myfile << this->spriteRect->x << endl;
                myfile << this->spriteRect->y;
            }
        };

        void modifyPosition()//ifstream &stream)
        {
            ifstream positionStream("resources/pozicija.txt");
            //char c = positionStream.get();

            if (positionStream.is_open()) {
                string line;
                int x;
                int y;
                int counter = 0;
                while (getline(positionStream, line))
                {
                    stringstream p(line);
                    if (counter == 1)
                    {
                        p >> x;
                        this->spriteRect->x += x;
                    }
                    else if ( counter == 2 )
                    {
                        p >> y;
                        this->spriteRect->y += y;
                    }
                    counter ++;
                    cout << line << endl;
                }
                positionStream.close();
            }
        };
};




#endif // REVERSEDSPRITE_H_INCLUDED



